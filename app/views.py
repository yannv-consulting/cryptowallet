# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django import template
from django.template.response import TemplateResponse
from app.models import Coin, Transaction
from app.transaction import TransactionForm
from pycoingecko import CoinGeckoAPI


@login_required(login_url="/login/")
def index(request):
    
    user = request.user
    cg = CoinGeckoAPI()

    transaction = Transaction.objects.filter(id_user=user.pk)

    solde = 0
    depenses = 0
    frais = 0

    for t in transaction:
        coin = t.id_coin
        solde += cg.get_price(str(coin.id_coin), 'eur')[str(coin.id_coin)]['eur'] * t.quantite
        depenses += t.total
        frais += t.frais

    portefeuille = {"solde": round(solde, 2), "depenses": round(depenses, 2), "frais": frais}

    top_coin = Coin.objects.filter(id_user=user.pk)

    for c in top_coin:
        c.etat = calculEtatCoin(user, c)
        c.depenses = calculTotalCoin(user, c)
        c.total = round(c.depenses + c.depenses * c.etat / 100, 2)
        c.save()

    top_coin = top_coin.order_by('-etat')[0:5]
    

    return TemplateResponse(request, 'index.html', {"user": request.user, "portefeuille": portefeuille, 'top_coin': top_coin})

@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:
        
        load_template      = request.path.split('/')[-1]
        context['segment'] = load_template
        
        html_template = loader.get_template( load_template )
        return HttpResponse(html_template.render(context, request))
        
    except template.TemplateDoesNotExist:

        html_template = loader.get_template( 'page-404.html' )
        return HttpResponse(html_template.render(context, request))

    except:
    
        html_template = loader.get_template( 'page-500.html' )
        return HttpResponse(html_template.render(context, request))

@login_required(login_url='/login/')
def mestransactions(request):
    
    user = request.user
    transaction = Transaction.objects.filter(id_user=user.id)

    for t in transaction:
        t.etat = str(calculEtatTransaction(t)) + ' %'

    return TemplateResponse(request, 'mestransactions.html', {"user": request.user, "transaction": transaction})

@login_required(login_url='/login/')
def transaction(request):
    form = TransactionForm(request.POST or None)

    msg = None

    cg = CoinGeckoAPI()

    coin_list = cg.get_coins_list()

    if request.method == "POST":

        if form.is_valid():
            id_coin = form.cleaned_data['nom']

            nom = next((item for item in coin_list if item["id"] == id_coin))['name']

            prix_coin = form.cleaned_data.get("prix_coin")
            quantite = form.cleaned_data.get("quantite")
            total = form.cleaned_data.get("total")
            date = form.cleaned_data.get("date")
            frais = form.cleaned_data.get("frais")

            if Coin.objects.filter(id_coin=id_coin).exists():
                coin = Coin.objects.get(id_coin=id_coin)

            else:
                coin = Coin.objects.create(id_user=request.user.pk, id_coin=id_coin, nom=nom)


            transaction = Transaction.objects.create(id_user=request.user.pk, id_coin=coin, nom=nom, prix_coin=prix_coin, quantite=quantite, date=date, total=total, frais=frais)
            
            return mestransactions(request)

        else:
            msg = 'Error validating the form'    

    return render(request, "transaction.html", {"form": form, "msg" : msg})


@login_required(login_url='/login/')
def profil(request):
    
    user = request.user

    return TemplateResponse(request, 'profil.html', {"user": user})


@login_required(login_url='/login/')
def coinView(request, id_coin):
    
    user = request.user
    cg = CoinGeckoAPI()

    coin = Coin.objects.get(id_coin=id_coin)
    transaction = Transaction.objects.filter(id_coin=coin)

    total = 0

    for t in transaction:

        t.etat = str(calculEtatTransaction(t)) + ' %'
        nom = t.nom

        total = cg.get_price(str(coin.id_coin), 'eur')[str(coin.id_coin)]['eur'] * t.quantite

    coin.etat = calculEtatCoin(user, coin)

    infos_coin = {"prix": cg.get_price(str(id_coin), 'eur')[str(id_coin)]['eur'], "total": total}



    return TemplateResponse(request, 'coin.html', {"user": request.user, "nom": nom, "transaction": transaction, "infos_coin": infos_coin, "coin": coin})




def calculEtatTransaction(transaction):
    cg = CoinGeckoAPI()

    coin = transaction.id_coin

    valeur_coin = cg.get_price(str(coin.id_coin), 'eur')[str(coin.id_coin)]['eur']

    etat = round(((valeur_coin - transaction.prix_coin)/transaction.prix_coin)*100, 2)

    return etat


def calculEtatCoin(user, coin):

    quantite_total = 0
    prix_total = 0

    cg = CoinGeckoAPI()


    transaction = Transaction.objects.filter(id_user=user.pk, id_coin=coin)
    valeur_coin = cg.get_price(str(coin.id_coin), 'eur')[str(coin.id_coin)]['eur']


    for t in transaction:
        quantite_total += t.quantite
        prix_total += t.total

    prix_coin = prix_total / quantite_total

    etat = round(((valeur_coin - prix_coin)/prix_coin)*100, 2)

    return etat

def calculTotalCoin(user, coin):

    total = 0

    transaction = Transaction.objects.filter(id_user=user.pk, id_coin=coin)

    for t in transaction:

        total += t.total

    return total
