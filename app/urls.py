# -*- encoding: utf-8 -*-

from django.urls import path, re_path
from app import views

urlpatterns = [

    # The home page
    path('', views.index, name='home'),
    path('mestransactions/', views.mestransactions, name='mestransactions'),
    path('transaction/', views.transaction, name='transaction'),
    path('profil/', views.profil, name='profil'),
    path('coin/<str:id_coin>/', views.coinView, name='coinView'),

    # Matches any html file
    re_path(r'^.*\.*', views.pages, name='pages'),

]
