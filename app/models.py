# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Coin(models.Model):
    id_user = models.IntegerField()
    id_coin = models.CharField(max_length=25)
    nom = models.CharField(db_column='Nom', max_length=25)  # Field name made lowercase.
    depenses = models.FloatField(db_column='Depenses')  # Field name made lowercase.
    total = models.FloatField(db_column='Total')  # Field name made lowercase.
    etat = models.FloatField(db_column='Etat', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Coin'


class Transaction(models.Model):
    id_user = models.IntegerField(blank=True, null=True)
    id_coin = models.ForeignKey(Coin, models.DO_NOTHING, db_column='id_coin')
    nom = models.CharField(db_column='Nom', max_length=25, blank=True, null=True)  # Field name made lowercase.
    total = models.FloatField(db_column='Total', blank=True, null=True)  # Field name made lowercase.
    prix_coin = models.FloatField(db_column='Prix_Coin', blank=True, null=True)  # Field name made lowercase.
    quantite = models.FloatField(db_column='Quantite', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date')  # Field name made lowercase.
    frais = models.FloatField(db_column='Frais', blank=True, null=True)  # Field name made lowercase.
    etat = models.CharField(db_column='Etat', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Transaction'
