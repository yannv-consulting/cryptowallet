# -*- encoding: utf-8 -*-

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from app.models import Transaction
from pycoingecko import CoinGeckoAPI

class TransactionForm(forms.Form):

    # getCoinsName()

    cg = CoinGeckoAPI()

    coin_list = cg.get_coins_list()

    coin_list2 = []


    for coin in coin_list:
        # print(coin['id'])
        coin_list2.append((coin['id'], coin['name']))


    nom = forms.ChoiceField(choices= coin_list2)
    prix_coin = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                # "placeholder" : "Prix par coin",                
                "class": "form-control"
            }
        ))
    quantite = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                # "placeholder" : "Quantité",                
                "class": "form-control"
            }
        ))
    total = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                # "placeholder" : "Prix",                
                "class": "form-control"
            }
        ))
    frais = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                # "placeholder" : "Frais",                
                "class": "form-control"
            }
        ))
    date = forms.DateTimeField(
        widget=forms.DateInput(
            attrs={
                # "placeholder" : "Date",                
                "class": "form-control"
            }
        ))


    class Meta:
        model = Transaction
        fields = ('nom', 'prix_coin', 'quantite', 'total', 'frais', 'date')


def getCoinsName():

    cg = CoinGeckoAPI()

    coin_list = cg.get_coins_list()

    coin_list2 = ()


    for coin in coin_list[0:2]:
        # print(coin['id'])
        coin_list2.append((coin['id'], coin['name']))

    print(coin_list2)